// Fill out your copyright notice in the Description page of Project Settings.


#include "WallElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "WallBase.h"  // ���������� ������������ ���� ������
#include "SnakeBase.h" // �� �������� ����������

// Sets default values
AWallElementBase::AWallElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

// ������ ������� ��� ���������, ����� ������� ������� �������
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly); // ������ MeshComponent-� ������ �������� ������ �������� �� overlap (����������), �� ��� ������
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap); // ����� �� ���� ������� � ������ ������������ - ����� overlap

}

// Called when the game starts or when spawned
void AWallElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)  // ���������, ��� ����� ��������� ������
	{
		auto Snake = Cast<ASnakeBase>(Interactor); // ������� �������� � ������
		if (IsValid(Snake)) // ��������� ���������� ��������� �� ����������
		{
			Snake->PlayDead(); // "����!"
			Snake->Destroy(); // ���������� � ������ ������-�����, �� �������� ����� ���� �������
			 
		}
	}

}