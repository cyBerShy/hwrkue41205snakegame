// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WallBase.generated.h"

class AWallElementBase;

UCLASS()
class HWRKUE41205SNAKEGAME_API AWallBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallBase();

	UPROPERTY(EditDefaultsOnly)
	float WallElementSize; // ������ �������� �����

	UPROPERTY(EditDefaultsOnly) // �������� � ����������
	TSubclassOf<AWallElementBase> WallElementClass; // ���������� � �������, � ������� ����� ��������� WallElement


	UPROPERTY() // �� �������� � ����������
	TArray<AWallElementBase*> Wall;  // ������ ���������� �� �������� �����


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// ���������� �������
	UFUNCTION(BlueprintCallable)
	void SetWall(int ElementsNum = 8); // ����� �������� ��������� �����

};
