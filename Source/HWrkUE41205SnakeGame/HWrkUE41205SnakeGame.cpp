// Copyright Epic Games, Inc. All Rights Reserved.

#include "HWrkUE41205SnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HWrkUE41205SnakeGame, "HWrkUE41205SnakeGame" );
