// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HWrkUE41205SnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HWRKUE41205SNAKEGAME_API AHWrkUE41205SnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
