// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h" // �� �������� ����������
#include "Math/UnrealMathUtility.h" // ����� ��� �������

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)  // ���������, ��� ��� ��������� ������
	{
		auto Snake = Cast<ASnakeBase>(Interactor); // ������� �������� � ������
		if (IsValid(Snake)) // ��������� ���������� ��������� �� ����������
		{
			Snake->AddSnakeElement(); // ���������� � ������ ���������� �������� ������,
			                          // ���������� ����� �� ���������, ��� ��� � ��� ��� ���������� �������� �� ���������
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, TEXT("Yum!")); // ���������, ��� ��� �������

			Snake->Score++; // ���� �������, ����������� ���������� � ���������� ��������� ���
			FoodMove(); // ������������ ���

//			Destroy(); // ������� ��� �� ����� �� �����
		}

	}
	else
		FoodMove(); // ������������ ���, ���� ����������� �� � �������, �� �� �������� =))
}

void AFood::FoodMove() // ����� ����������� ��� � ����� �������
{

	auto NewFoodX = FMath::RandRange(-7, 7); // ��������� ���������� �� X, ��������� � �������� ����
	auto NewFoodY = FMath::RandRange(-7, 7); // ��������� ���������� �� Y, ��������� � �������� ����
	FVector FoodLoc = FVector(NewFoodX * 60, NewFoodY * 60, 1); // ��������� ������ �������, ���������, �� ������� ������ ����
	SetActorLocation(FoodLoc); // ������������� ��� �� ������� �������

}

