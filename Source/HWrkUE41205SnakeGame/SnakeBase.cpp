// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h" // ���������� ������� ������
#include "Interactable.h"  // ���������� ��� ����� ����������

// Sets default values
ASnakeBase::ASnakeBase()  // ����������� ������
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f; // ������ �������� ������
	MovementSpeed = 10.f; // �������� �������� ������ �� ��������� (����� ������)
	LastMoveDirection = EMovementDirection::DOWN; // ����������� �������� ������ �� ��������� (����)
	Moving = false; // �� �������� ��������� ���������� ������� ����������
	Score = 0; // �������� �������, ���������� � ����������� �������� ���

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed); // ������ ������� ������� ����������� ��� � MovementSpeed,
	                                     // ������� ��������� �����, ������� ��� � ������� ����� ��������
	                                     // �� ElementSize, ��� ����������� ������� ��������
	AddSnakeElement(5); // ��������� �� ������ ����� 5 ��������� ������
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(); // �������� �� ���� ����� �������� ������

}

void ASnakeBase::AddSnakeElement(int ElementsNum) // ������������� ������ ���������� �������� ������
{
	// ������ ���������� ��������� ������ SnakeElements.Num(), ������ �������� ������ �������� �� ������� ���������� ���������

	for (int i = 0; i < ElementsNum; ++i)  // ���������� ������ ���������� ��������� ������
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0); // ��������� ������� ������� ��������
		if (SnakeElements.Num() > 0)  // ���� ���� �� ���� ������� ����������, ��������� ����� �� ������� ���������� ��������
		{
			auto CurrentElement = SnakeElements[SnakeElements.Num() - 1]; // �������� ����� ���������� �������� ������
			NewLocation = CurrentElement->GetActorLocation(); // �������� ������� ���������� �������� ������
		}
		FTransform NewTransform(NewLocation); // �������������� "���������"
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform); // ������� ����� �������
		NewSnakeElem->SnakeOwner = this; // ��� ���������� ������ ����� ������������� ������� ��������� � this
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem); // ��������� ����� ������� � ������ � �������� ���������� ��� ���������� �����
		if (ElemIndex == 0) // ���������� ������ �� �������� ������ �������� � ������� ��������� ���� ������
		{
			NewSnakeElem->SetFirstElementType(); // �������� ����� ���������� ������� ��������
		}
	}

}

void ASnakeBase::Move() // ���������� ������ �������� ������
{
	FVector MovementVector(ForceInitToZero); // ��������� ������ �������� � ����� �������������� ��� �������� ����������

	switch (LastMoveDirection)  // �������� ����������� ��������
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize; // ��������, ������ ������� �������� ������
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break; 
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();  // ��������� �������� ��������������� ����� ���������

	for (int i = SnakeElements.Num() - 1; i > 0; i--) // ������� ������ � ������, �������� ����� ���������� ��������
	{
		auto CurrentElement = SnakeElements[i];  // ������� ������� 
		auto PrevElement = SnakeElements[i - 1]; // ���������� ������� (� ���� ��������� ����������)
		FVector PrevLocation = PrevElement->GetActorLocation(); // �������� ����� � ������������ � ����������� ��������, ����� � ��� �������� ��������� �������
		CurrentElement->SetActorLocation(PrevLocation); // ������� ������� ������� �� ����� �����������,
		                                                // �� ����, ��������� �������� ����� ����������,
		                                                // �� ��������� ������� ������ �������������� ������� � ��������� �����
//		SnakeElements[i]->MeshComponent->SetVisibility(true); // ������ ������� �������
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector); // ������� ������ ������ (�������� �� ����)
//	SnakeElements[0]->MeshComponent->SetVisibility(true);  // ������ ������ �������
	SnakeElements[0]->ToggleCollision();  // �������� �������� ������� ����� ����� ��������

	Moving = false; // ������� ���� ��������, �������� ��������, ����� ����� ������ ����������� ��������.

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) // ��������� ��������� �� ����������
	{
		int32 ElemIndex; // ���������� � ������� ���������� ��� ������
		SnakeElements.Find(OverlappedElement, ElemIndex); // ������� ������ ���������� ��������
		bool bIsFirst = ElemIndex == 0; // ���� ������ = 0, �� ��� ������� �������� �������
		IInteractable* InteractableInterface = Cast<IInteractable>(Other); // ������� ��������� �� ��� ���������,
		                                                                   // ����� cast �������� ��� ����� � ����� ����������
		                                                                   //(�������� ������ ������ � ���������)
		if (InteractableInterface) // ��������� ������ �� ������������, �� ��������� �� �����, ������� ������ ��������� ��� ��������� �� �������
		{
			// ��� ��� ���� ��� ��������� ����� �� ����� � ����������, ���������� ����������� �������� ��� �++
			// ����� ������� �� ������� ���� ��������� ������ �� ���������� ������� ���������� � ������ ����� ����� ����������
			// �������� ��� ����� Interact � �������� � ���� ������, ������� ������������� � �������, ��������������� ������ ���������
			// ��������� ������ ��������, ����� ��������, ��� �� ������� ������� ������ ��� ���
			//
			InteractableInterface->Interact(this, bIsFirst);
		}
	}

}

void ASnakeBase::PlayDead()
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 7.0f, FColor::Red, TEXT("!!! GAME OVER !!!")); // ���������, ����� �� ������� �������� �� ���������
	    GEngine->AddOnScreenDebugMessage(-1, 8.0f, FColor::Green, FString::Printf(TEXT("Collected %d food!"), Score)); // ����� ���������� �����

}

int ASnakeBase::GetScore() // ������ ���������� �������� ���
{
	return Score;
}

