// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBase.h"
#include "WallElementBase.h" // ���������� ������� �����
#include "Interactable.h"    // ���������� ��� ����� ����������

// Sets default values
AWallBase::AWallBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WallElementSize = 100.f; // ������ �������� �����

}

// Called when the game starts or when spawned
void AWallBase::BeginPlay()
{
	Super::BeginPlay();

	SetWall(16);

}

// Called every frame
void AWallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallBase::SetWall(int WallElementsNum) // ������������� ������ ���������� �������� �����
{

	int SetStartX = WallElementsNum / 2; // ��������� �� ������ �� �������� ������ ����� �� X
	int SetStartY = WallElementsNum / 2; // ��������� �� ������ �� �������� ������ ����� �� Y

	FVector NewLocation(SetStartX * WallElementSize, SetStartY * WallElementSize, 1); // ��������� ������� ������ �����

	for (int i = 0; i < WallElementsNum; ++i)  // ���������� ������ ���������� ��������� �����
	{

		FTransform NewTransform(NewLocation); // �������������� "���������"
		AWallElementBase* NewWallElem = GetWorld()->SpawnActor<AWallElementBase>(WallElementClass, NewTransform); // ������� ����� �������
		NewWallElem->WallOwner = this; // ��� ���������� ������ ����� ������������� ������� ��������� � this
		int32 ElemIndex = Wall.Add(NewWallElem); // ��������� ����� ������� � ������ � �������� ���������� ��� ���������� �����

		NewLocation.X -= WallElementSize;// ���� ���� �� ��� X

	}

	for (int i = 0; i < WallElementsNum; ++i)  // ���������� ������ ���������� ��������� �����
	{

		FTransform NewTransform(NewLocation); // �������������� "���������"
		AWallElementBase* NewWallElem = GetWorld()->SpawnActor<AWallElementBase>(WallElementClass, NewTransform); // ������� ����� �������
		NewWallElem->WallOwner = this; // ��� ���������� ������ ����� ������������� ������� ��������� � this
		int32 ElemIndex = Wall.Add(NewWallElem); // ��������� ����� ������� � ������ � �������� ���������� ��� ���������� �����

		NewLocation.Y -= WallElementSize; // ���� ���� �� ��� Y

	}

	for (int i = 0; i < WallElementsNum; ++i)  // ���������� ������ ���������� ��������� �����
	{

		FTransform NewTransform(NewLocation); // �������������� "���������"
		AWallElementBase* NewWallElem = GetWorld()->SpawnActor<AWallElementBase>(WallElementClass, NewTransform); // ������� ����� �������
		NewWallElem->WallOwner = this; // ��� ���������� ������ ����� ������������� ������� ��������� � this
		int32 ElemIndex = Wall.Add(NewWallElem); // ��������� ����� ������� � ������ � �������� ���������� ��� ���������� �����

		NewLocation.X += WallElementSize; // ���� ���� �� ��� X

	}

	for (int i = 0; i < WallElementsNum; ++i)  // ���������� ������ ���������� ��������� �����
	{

		FTransform NewTransform(NewLocation); // �������������� "���������"
		AWallElementBase* NewWallElem = GetWorld()->SpawnActor<AWallElementBase>(WallElementClass, NewTransform); // ������� ����� �������
		NewWallElem->WallOwner = this; // ��� ���������� ������ ����� ������������� ������� ��������� � this
		int32 ElemIndex = Wall.Add(NewWallElem); // ��������� ����� ������� � ������ � �������� ���������� ��� ���������� �����

		NewLocation.Y += WallElementSize; // ���� ���� �� ��� Y

	}

}